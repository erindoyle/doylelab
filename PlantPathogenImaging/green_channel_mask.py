#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 11:32:48 2017

@author: Allison Morrill

The purpose of this code is to create a mask based on the green channel of our
image. This mask will then be applied to our Plant Mask Image in order to find
an accurate green pixel count of our plant.

This section of code was then added to FindingArea.py
"""

import cv2, sys, numpy as np

# read command-line arguments
filename = sys.argv[1]
k = int(sys.argv[2])

# Load the original image
img = cv2.imread(filename) 

#Obtain blue channel from image
gchan =img[:,:,1]

cv2.namedWindow("Green Channel Image", cv2.WINDOW_NORMAL)
cv2.imshow("Green Channel Image", gchan)
cv2.waitKey(0)

blur = cv2.GaussianBlur(gchan, (k, k), 0)

"""
This section of code is used to mask the plant.
This mask will then be applied back to the original image which will allow us 
to get an accurate green pixel count as non-plant green pixel values will not 
be included in our count_nonzero(greens) pixel count.

"""
#Threshold image to turn only white size marker pixels on
(t, maskLayer) = cv2.threshold(blur, 85, 255, cv2.THRESH_BINARY)

# make a mask suitable for color images
plantgreens_mask = cv2.merge([maskLayer, maskLayer, maskLayer])

# display the mask image
cv2.namedWindow("Plant Greens Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Plant Greens Mask", plantgreens_mask)
cv2.waitKey(0)

# Apply the mask and display the result
FinalPlantImg = cv2.bitwise_and(img, plantgreens_mask)
cv2.namedWindow("Final Plant Image", cv2.WINDOW_NORMAL)
cv2.imshow("Final Plant Image", FinalPlantImg)
cv2.waitKey(0)

# counts number of green pixels and displays results
greens = FinalPlantImg[:,:,1]
amtGreen = np.count_nonzero(greens)
print("Green pixel amount is", amtGreen)
 
# counts the total number of pixels
image = cv2.imread(filename)
print("Total pixel amount is",img.shape[0]*img.shape[1])