#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 09:25:03 2017

@author: Allison Morrill

This is a copy of the FindingArea.py script. This script is used for working
on the contour of the whole leaf, and once that section of code has been 
completed and seems to be working properly, then the section is to be added 
to the main code- FindingArea.py
"""

import cv2, sys, numpy as np
import math

# read command-line arguments
filename = sys.argv[1]

# Load the original image
img = cv2.imread(filename) 

#Obtain blue channel from image
bchan =img[:,:,0]

cv2.namedWindow("Blue Channel Image", cv2.WINDOW_NORMAL)
cv2.imshow("Blue Channel Image", bchan)
cv2.waitKey(0)

blur = cv2.GaussianBlur(bchan, (7, 7), 0)

"""
This section of code is used to mask the size marker.
Later in the script, the Size Marker Mask image will be contoured and produce
the contour area in pixels which can then be converted to cm^2.
"""
#Threshold image to turn only white size marker pixels on
(t, maskLayer) = cv2.threshold(blur, 200, 255, cv2.THRESH_BINARY)

# make a mask suitable for color images
sizemarker_mask = cv2.merge([maskLayer, maskLayer, maskLayer])

# display the mask image
cv2.namedWindow("Size Marker Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Size Marker Mask", sizemarker_mask)
cv2.waitKey(0)

"""
This section of code produces a mask that masks out the size marker.
This mask is then applied back to the original image to produce an image 
only containing the plant. Then, we find the green pixel count of this image.
"""

#Threshold image to turn white size marker pixels off
(t, maskLayer2) = cv2.threshold(blur, 150, 255, cv2.THRESH_BINARY_INV)

plant_mask = cv2.merge([maskLayer2, maskLayer2, maskLayer2])

# display the mask image
cv2.namedWindow("Plant Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Plant Mask", plant_mask)
cv2.waitKey(0)

# Apply the mask and display the result
maskedPlantImg = cv2.bitwise_and(img, plant_mask)
cv2.namedWindow("Masked Plant Image", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Plant Image", maskedPlantImg)
cv2.waitKey(0)

"""
Here we contour the Size Marker Mask image. We then find the contour area in 
pixels which can be converted to cm^2 using our conversion factor.
"""

(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)

# find contours
(_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)


"""
This section finds the contour of maximum size and sets that as the contour
associated with the size marker. This is then used to find the area of that  
max contour to be used in the conversion factor.
"""
# print table of contours and sizes
print("Found %d objects." % len(contours))
max = 0
max_i = 0
for (i, c) in enumerate(contours):
    if (max < len(c)):
        max = len(c)
        max_i = i
    print("\tSize of contour %d: %d" % (i, len(c)))

cnt = contours[max_i]

# draw contours over original image
cv2.drawContours(sizemarker_mask, contours, -1, (0, 0, 255), 5)

# display original image with contours
cv2.namedWindow("Contoured Size Marker", cv2.WINDOW_NORMAL)
cv2.imshow("Contoured Size Marker", sizemarker_mask)
cv2.waitKey(0)

area = cv2.contourArea(cnt)
print('The pixel area of the contour is ',area)

"""
Here, we create a custom mask that goes through all the pixels and determines
the RGB values and replaces those which the maximum RGB value is not G with 
zeros (black).

"""

for i in range(len(maskedPlantImg)):
    for x in range(len(maskedPlantImg[i])):
        max_index = np.argmax(maskedPlantImg[i][x])
        r_value = maskedPlantImg[i][x][2]
        g_value = maskedPlantImg[i][x][1]
        b_value = maskedPlantImg[i][x][0]
        if (max_index == 1):
            pass
        elif ((max_index == 2) and (r_value < 200)):
            maskedPlantImg[i][x] = [0,0,0]
        elif (max_index == 0):
            maskedPlantImg[i][x] = [0,0,0]

cv2.namedWindow("New Green Channel Image", cv2.WINDOW_NORMAL)
cv2.imshow("New Green Channel Image", maskedPlantImg)
cv2.waitKey(0)

##cv2.imwrite("newgchanimg.tiff",maskedPlantImg)

# counts number of green pixels and displays results
greens = maskedPlantImg[:,:,1]
amtGreen = (greens > 150).sum()
print("Green pixel amount is", amtGreen)
 
# counts the total number of pixels
image = cv2.imread(filename)
print("Total pixel amount is",img.shape[0]*img.shape[1])

"""
This section of code was developed to find area of green pixels in our 
plant/leaf image.
"""

# Set new size marker pixel area variable equal to calculated area of contour
Sizemarker_pixel_area = area

# Here, we use the area to solve for the radius
radius = math.sqrt(area/3.14) 
print('radius:',format(radius,'.2f'),'pixels')

"""
In this section of code, we create the conversion factor for pixels to cm^2.
This will then be used to convert the green pixel count to area in cm^2.

"""
conversion_factor = (0.9525/radius)**2
print('conversion factor:',format(conversion_factor,'.5f'))

pixel_to_squared_centimeter_conversion = (amtGreen*conversion_factor)
print('Green Leaf Area in cm^2:',
      format(pixel_to_squared_centimeter_conversion,'.2f'))

GreenLeafAreaCm = pixel_to_squared_centimeter_conversion

"""
In this section of code, we will find the area of the whole leaf by finding
the contour of the whole leaf.


THIS SECTION IS UNFINISHED WORK. 

"""

# create binary image from original image
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5, 5), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)

cv2.namedWindow("Grayscale of Original", cv2.WINDOW_NORMAL)
cv2.imshow("Grayscale of Original", blur)
cv2.waitKey(0)

# find contours
(_, contours, _) = cv2.findContours(binary, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

# print table of contours and sizes
print("Found %d objects." % len(contours))
max = 0
max_i = 0
for (i, c) in enumerate(contours):
    if (max < len(c)):
        max = len(c)
        max_i = i
    print("\tSize of contour %d: %d" % (i, len(c)))

WholeLeafcnt = contours[max_i]

# draw contours over original image
cv2.drawContours(img, contours, -1, (0, 0, 255), 5)

# display original image with contours
cv2.namedWindow("Whole Leaf Contour", cv2.WINDOW_NORMAL)
cv2.imshow("Whole Leaf Contour", img)
cv2.waitKey(0)

"""
Once the contour of the whole leaf has been found, the area of the contour is 
calculated, giving us the area of the whole leaf. Then, to find the area of 
infection, the green pixel area is subtracted from the whole leaf area.
"""

WholeLeafArea = cv2.contourArea(WholeLeafcnt)
print('The pixel area of the whole leaf contour is ',WholeLeafArea)

leaf_pixel_to_squared_centimeter_conversion = (WholeLeafArea*conversion_factor)
print('Whole Leaf Area in cm^2:',
      format(leaf_pixel_to_squared_centimeter_conversion,'.2f'))

WholeLeafAreaCm = leaf_pixel_to_squared_centimeter_conversion

InfectedAreaCm = WholeLeafAreaCm-GreenLeafAreaCm
print('The area in cm^2 of infection is: ',format(InfectedAreaCm,'.2f'))
