#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 13:51:19 2017

@author: Allison Morrill

This script was created to develop a conversion formula from area in pixels
to area in cm^2 based on the area of a known size marker.

This section of code was then added to FindingArea.py
"""
import math

area = 8385  
amtGreen = 58854

# putting in an area and amtGreen values for now, these lines of code will not
# be needed in FindingArea.py

# Set new size marker pixel area variable equal to calculated area of contour
Sizemarker_pixel_area = area

# Here, we use the area to solve for the radius
radius = math.sqrt(area/3.14) 
print('radius:',format(radius,'.2f'),'pixels')

"""
In this section of code, we create the conversion factor for pixels to cm^2.
This will then be used to convert the green pixel count to area in cm^2.

"""
conversion_factor = (0.9525/radius)**2
print('conversion factor:',format(conversion_factor,'.5f'))

pixel_to_squared_centimeter_conversion = (amtGreen*conversion_factor)
print('Area in cm^2:',format(pixel_to_squared_centimeter_conversion,'.2f'))