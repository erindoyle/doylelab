#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 09:36:46 2017

@author: Allison Morrill

The purpose of this script is to create a mask based on the blue channel
that will provide us with an image containing just the size marker.

We then invert the mask to obtain an image with just the plant (no size marker).

Using these two images, we can find the pixel area of the size marker based
on the contour and find the green pixel count of the plant image.

This section of code was then added to FindingArea.py
"""
import cv2, sys

# read command-line arguments
filename = sys.argv[1]
#k is the kernel size
k = int(sys.argv[2])

# Load the original image
img = cv2.imread(filename) 

#Obtain blue channel from image
bchan =img[:,:,0]

cv2.namedWindow("Blue Channel Image", cv2.WINDOW_NORMAL)
cv2.imshow("Blue Channel Image", bchan)
cv2.waitKey(0)

blur = cv2.GaussianBlur(bchan, (k, k), 0)

"""
This section of code is used to mask the size marker.
Later in the script, the Size Marker Mask image will be contoured and produce
the contour area in pixels which can then be converted to cm^2.
"""

#Threshold image to turn only white size marker pixels on
(t, maskLayer) = cv2.threshold(blur, 150, 255, cv2.THRESH_BINARY)

# make a mask suitable for color images
sizemarker_mask = cv2.merge([maskLayer, maskLayer, maskLayer])

# display the mask image
cv2.namedWindow("Size Marker Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Size Marker Mask", sizemarker_mask)
cv2.waitKey(0)

"""
This section of code produces a mask that masks out the size marker.
This mask is then applied back to the original image to produce an image 
only containing the plant. Then, we find the green pixel count of this image.
"""

#Threshold image to turn white size marker pixels off
(t, maskLayer2) = cv2.threshold(blur, 150, 255, cv2.THRESH_BINARY_INV)

plant_mask = cv2.merge([maskLayer2, maskLayer2, maskLayer2])

# display the mask image
cv2.namedWindow("Plant Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Plant Mask", plant_mask)
cv2.waitKey(0)

# Apply the mask and display the result
maskedPlantImg = cv2.bitwise_and(img, plant_mask)
cv2.namedWindow("Masked Plant Image", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Plant Image", maskedPlantImg)
cv2.waitKey(0)

