READ ME file for the Plant Pathogen Project - Jordan Zonner

Input Images Used Thus Far:




Inoculation Images:



Leaf Images with Damage/Discoloring:




-----------------------------------------

Scripts:
FindingArea_JZ.py
Whole_leaf_contour_JZ.py
blue_channel_mask_JZ.py
green_channel_mask_JZ.py
ConversionFactor_JZ.py


blue_channel_mask_JZ.py
-Script that was taken from the original doylelab repo. It will currently run on these images: 
Kitake_ROX1_Leaf_Gen1_002_7.7.17.tiff
Nipponbare_Leaf2_Gen2_006_7.7.17.tiff
Nipponbare_Leaf_Gen1_012_7.7.17.tiff
Nipponbare_Leaf_Gen2_006_7.7.17.tiff


green_channel_mask_JZ.py
-Script that works on these images: However, it will not print out the amtGreen variable.
Kitake_ROX1_Leaf_Gen1_002_7.7.17.tiff
Nipponbare_Leaf2_Gen2_006_7.7.17.tiff
Nipponbare_Leaf_Gen1_012_7.7.17.tiff
Nipponbare_Leaf_Gen2_006_7.7.17.tiff


ConversionFactor_JZ.py
-Script that seems to have no current issues. 

Whole_leaf_contour_JZ.py
-Script from Allison Morrill that will be added to FindingArea_JZ.py when it is working properly. 




Daily Logs:
12/07/17 (Jordan)
-Went through the individual pieces of code and ran some of the images of both rice and tobacco, to find that specifically green_channel_mask_JZ.py is still not printing out the counted amtGreen variable. I am not sure if these values are printing on top of the image, or if something is wrong before the computer begins to count the green pixels. 

12/08/17
-Whole_leaf_contour_JZ.py works with Inculated_Tobacco_Leaf_004_Aug4_Day5.tiff and Rice_Leaf_With_Dot.tiff. Some notes: blue channel will provide us with an image that contains just the size marker, then it is supposed to invert the mask with just the plant and without the size marker, however it is not showing this image with the two previously used images today. I am going to try more images.
-Read through conversionFactor.py
-Look through the error on line 78, and figure what that error is meaning.

