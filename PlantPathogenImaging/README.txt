Post code for the Plant Pathogen Imaging project and images needed to run it to this folder. 
"""

READ ME File for Pathogen project - Allison Morrill


Input Images Used Thus Far:
Greensquare.JPG
Rice_Plant_Leaf_001_June23.tiff
Rice_Plant_Leaf_002_June23.tiff
Rice_Plant_Leaf_003_June23.tiff
IR-64_Leaves_Gen2_008_June16.tiff

(Inoculation Images)
InoculatedLeafSample.png
Inoculated_Tabacco_Leaf_003_Aug4.tiff
Inoculated_Tabacco_Leaf_001_Day5_Aug4.tiff
Inoculated_Tabacco_Leaf_004_Aug4_Day5.tiff
Inoculated_Tabacco_Leaf_Day5_002_Aug4.tiff

(Leaf Images with Damage/Discoloring)
Kitake_ROX1_Leaf_Gen1_002_7.7.17.tiff
Nipponbare_Leaf2_Gen2_006_7.7.17.tiff
Nipponbare_Leaf_Gen1_012_7.7.17.tiff
Nipponbare_Leaf_Gen2_006_7.7.17.tiff
Random_Greenhouse_Plant_Leaf_Backside_7.7.17.tiff
Random_Greenhouse_Plant_Leaf_Frontside_7.7.17.tiff
Rice_Leaf_With_Dot.tiff
Slight_Yellowing_Rice_Leaf.tiff
------------------------------------------------------

Scripts:
FindingArea.py (MAIN CODE) - 
This is the complete code created to find the green plant area in cm^2 of our plant or leaf images. It includes sections of code from blue_channel_mask.py, green_channel_mask.py, and 

ConversionFactor.py.
Whole_leaf_contour.py (COPY OF MAIN CODE USED FOR WORKING ON CONTOURS)
-Copy of FindingArea.py script created to work on contouring the outermost edge of the leaf.

blue_channel_mask.py
-Script created to mask size marker based on blue channel of plant image.  Inverse of mask also created to produce plant image without size marker.

green_channel_mask.py
-Script created to mask green plant pixels without non-plant green pixel values showing up in background of image. This code was produced to more accurately to find the green pixel count of the plant.                                                                                                                                                                                                                                                                         

ConversionFactor.py
-Script created to develop the conversion factor of area in pixels to cm^2.  This code then finds the area in cm^2 of the green pixels in the image.


-------------------------------------------------------

Daily Logs:
6/29/2017 (Allison)
-FindingArea.py, blue_channel_mask.py, green_channel_mask.py, and ConversionFactor.py scripts were created.  FindingArea.py script runs smoothly with both whole plant and individual leaf images. May want to use adaptive thresholding instead of simple thresholding.

7/7/17 (Allison)
-Six new images of damaged leaves were added to folder. These images were given a test run through FindingArea.py, but we ran into problems when leaves contained very light/white spots. When more than one contour is drawn, the wrong contour area is used in the conversion factor, throwing off green leaf area. May need to work with threshold/contouring to fix this.

7/12/17 (Allison)
Updated FindingArea.py:
-Updated code to find the contour of maximum size and set that as the contour associated with the size marker. This ensures that the correct size marker area value is used in the conversion factor.
-Create a custom mask that goes through all the pixels and determines the RGB values and replaces those which the maximum RGB value is not G with zeros or black.

7/18/17 (Allison)
-Added Greensquare.JPG image to folder. This image contains a green square about 1.2cmx1.2cm.
Using this image, a test run of the code FindingArea.py was made to make sure the green pixel count is accurate. The code produced an area of 1.46cm^2, telling us that the code is fairly accurate. We believe the test run of code FindingArea.py was successful.

7/21/17 (Allison)
-Added Rice_Leaf_With_Dot.tiff and Slight_Yellowing_Rice_Leaf.tiff to folder. Would like to see how code contours these images of leaves with slight damage.

7/27/17 (Allison)
-Added Whole_leaf_contour.py to folder.  This code is a copy of FindingArea.py that was made in order to work on finding a contour of the outermost edge of the whole leaf.  So far, I have been testing the code as I go on the images Rice_Leaf_With_Dot.tiff and Random_Greenhouse_Plant_Leaf_Backside_7.7.17.tiff.  The code makes a contour around the outermost edge of the rice leaf, but does not produce one contour around the outermost edge of the damaged leaf and instead creates multiple contours inside of the leaf. Work still needs to be done on the contouring of the whole leaf.

8/1/17 (Allison)
-Work was done on Whole_leaf_contour.py. Added section that prints table of the contours and their sizes.  The goal is to be able to create a contour around the outermost edge of the whole leaf and then take the largest contour from the table (as this should correspond with the whole leaf contour) and find its area. This would give us an area of the whole leaf that we can then subtract the green pixel area from in order to find the area of infection.  However, the problem with contouring the damaged leaf has still not been fixed. Have tried using both cv2.RETR_EXTERNAL and cv2.RETR_TREE. 

8/1/17 (Allison)
-Added InoculatedLeafSample.png to folder. This image is from the paper "A Pseudomonas syringae pv. tomato DC3000 mutant lacking the type III effector HopQ1-1 is able to cause disease in the model plant Nicotiana benthamiana." -Wei et al
-Tried to run InoculatedLeafSample.png through FindingArea.py. However, there were a few problems.
The image does not have a size marker, but it did have labels printed in white on the different inoculation areas. Thus the code contours the white labels and did not have a contour for a size marker. Area for green pixels would not be accurate for this picture. Plan to look at color values of infected areas of this image in ImageJ.

8/2/17 (Allison)
-Added Tobacco_Testing_Leaf_001.tiff. When run through Whole_leaf_contour.py, the contour produced was still not correct.

8/2/17 (Allison)
Updated Whole_leaf_contour.py
-Section of code was added to the end of the script to find the final area of infection. Code was written to find whole leaf contour area and then subtract the green pixel area from it to find the area of infection. However, when I ran the image Rice_Leaf_With_Dot.tiff, the code produced a negative area.  At this point the section of code that counts the green pixel area may not be as accurate as we had originally thought. Work still needs to be done on the whole leaf contour section as this section works well with images of rice leaves, but runs into problems with tobacco leaves.

8/3/17 (Allison)
-Added following images of inoculated tobacco leaves.
Inoculated_Tabacco_Leaf_003_Aug4.tiff
Inoculated_Tabacco_Leaf_001_Day5_Aug4.tiff
Inoculated_Tabacco_Leaf_004_Aug4_Day5.tiff
Inoculated_Tabacco_Leaf_Day5_002_Aug4.tiff










