#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 13:23:40 2017

@author: diva
"""

import cv2, numpy as np,sys

# read command-line arguments
filename = sys.argv[1]
t = int(sys.argv[2])

# Load the original image
img = cv2.imread(filename) 

# Create the basic black image 
mask = np.zeros(img.shape, dtype = "uint8")

# Draw a white, filled circle on the mask image
cv2.circle(mask,(192,834),60,(255,255,255),-1)

# Apply the mask and display the result
maskedImg = cv2.bitwise_and(img, mask)
cv2.namedWindow("Masked Image", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Image", maskedImg)
cv2.waitKey(0)

# create binary image
gray = cv2.cvtColor(maskedImg, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5, 5), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)

# find contours
(_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE) 


# print table of contours and sizes
print("Found %d objects." % len(contours))
for (i, c) in enumerate(contours):
    print("\tSize of contour %d: %d" % (i, len(c)))

# draw contours over original image
cv2.drawContours(maskedImg, contours, -1, (0, 0, 255), 5)

# display original image with contours
cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", maskedImg)
cv2.waitKey(0)

cnt = contours[0]

area = cv2.contourArea(cnt)
print('The area of the contour is ',area)


#count_non_zero = np.count_nonzero(maskedImg)
#print("The area of green pixels is",count_non_zero)